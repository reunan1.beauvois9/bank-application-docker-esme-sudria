Just cd into directory and run:

docker-compose up -d

to stop:

docker-compose down

NOTE: The database will automatically be created.

To access the database, enter in your web browser localhost:8081 with "db" as server, "root" as user, "example" as password and "BankApp" as database. Do this only if you need. 

To access the app you must enter in your web browser localhost:81.
