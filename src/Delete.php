<?php
	$connection = new mysqli("db","root","example","BankApp"); // it works
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <title>Delete an account</title>

<link rel="stylesheet" href="tools2.css"/>
<p class="img1"><img width=120px src="images2/esme.bmp"/></p>
</head>

<body>
    <center><h1>Erasure of an existing account</h1></center>

    <center><aside>
    <br>
    <a href=index.php><img src="images2/MainPage.bmp"/></a>
    <a href=Authentication.php><img src="images2/Authentication.bmp"/></a>
    <a href=Create.php><img src="images2/Create.bmp"/></a>
    <a href=Edit.php><img src="images2/Edit.bmp"/></a>
    <a href=Observe.php><img src="images2/Observe.bmp"/></a>
    <a href=Delete.php><img src="images2/Delete.bmp"/></a>
    <br><br>
    </aside></center>

    <center><h2>Delete an account</h2></center>

    <form method="post">

    <center><label for="id">ID : </label>
    <input class="input2" type="text" name="idText" value=""></center>

    <br>

    <center><input class="input3" type=submit border-radius:10px name="Delete" value="Delete"></center><br>

    <?php

        $array1=array();
        $value1=0;
        $error1=0;

        $id=mysqli_query($connection,"SELECT ID FROM MonetaryData WHERE 1");
        while($ligne1=$id->fetch_assoc()){$array1[$value1]=$ligne1["ID"];$value1++;}

        

        if(isset($_POST["Delete"]))
        {

            for($i=0;$i<$value1;$i++){if(($_POST['idText'])==($array1[$i]))$error1=1;}

            if($_POST['idText']==NULL)
            {
                echo '<font color="red">To delete an account you must enter its ID.</font>';
            }
            if($_POST['idText']!=NULL && $error1==0)
            {
                echo '<font color="red">This ID "'.$_POST['idText'].'" is not used.</font>';
            }
            if($_POST['idText']!=NULL && $error1==1)
            {
                mysqli_query($connection,"DELETE FROM MonetaryData WHERE ID='".$_POST['idText']."'");
                echo '<font color="blue">The account "'.$_POST['idText'].'" has been deleted.</font>';
            }
        }
    ?>
<p class="p2">ESME Sudria, 38th street Molière, Paris-Sud Ivry, Tel : 01.56.20.62.00</p>
</body>

</html>
